<?php

use yii\db\Migration;

/**
 * Class m180624_081035_init_rbac
 */
class m180624_081035_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    $auth = Yii::$app->authManager;
      $employee = $auth->createRole('employee');
      $auth->add($employee);

      $manager = $auth->createRole('manager');
      $auth->add($manager);
              
      $auth->addChild($manager, $employee);

      $viewTask = $auth->createPermission('viewTask');
      $auth->add($viewTask);

      $createTask = $auth->createPermission('createTask');
      $auth->add($createTask);   

 $updateTask = $auth->createPermission('updateTask');
      $auth->add($updateTask);      

       $deleteTask = $auth->createPermission('deleteTask');
      $auth->add($deleteTask); 

       $viewUser = $auth->createPermission('viewUser');
      $auth->add($viewUser);   

       $updateUser = $auth->createPermission('updateUser');
      $auth->add($updateUser);      
         
              
     $updateOwnUser = $auth->createPermission('updateOwnUser');

     $rule = new \app\rbac\UserRule;
      $auth->add($rule);
              
     $updateOwnUser->ruleName = $rule->name;                
     $auth->add($updateOwnUser);                 
                                
              
      $auth->addChild($manager, $deleteTask);
       $auth->addChild($manager, $createTask);
        $auth->addChild($manager, $updateTask);
         $auth->addChild($manager, $viewTask);
          $auth->addChild($manager, $updateUser); 
           $auth->addChild($employee, $viewUser); 

      $auth->addChild($updateOwnUser, $updateUser);
    
     $auth->addChild($employee, $updateOwnUser); 


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_081035_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_081035_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
