<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $urgency
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urgency', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

   public function behaviors() {
        return [
             BlameableBehavior::className(),
           
        ];
    }
    

        
  
    
    public function getUrgency1()
    {
        return $this->hasOne(Urgency::className(), ['id' => 'urgency']);
    }    
    
    public function getName1()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }     

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }
}
