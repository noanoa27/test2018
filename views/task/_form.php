<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper; //בדרופדאון
use app\models\Urgency; 
use app\models\User; 

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

  
  <?= $form->field($model, 'name')->dropDownList(
             ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name')) 
  
    ?>

      <?= $form->field($model, 'urgency')->dropDownList(
             ArrayHelper::map(Urgency::find()->asArray()->all(), 'id', 'name')) 
  
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
